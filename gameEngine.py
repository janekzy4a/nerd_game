
import numpy as np
import time
import importlib
import game

NUMBER_OF_PLAYERS = 4
playerPoints = np.zeros(NUMBER_OF_PLAYERS)
whoseTurn=0
winner=-1

def calculate_vote_result(playerVotes, NUMBER_OF_PLAYERS):
    if sum(playerVotes) > NUMBER_OF_PLAYERS *1.5:
        return True
    else:
        return False
def make_decision():
    dec=0
    while (dec != '1' and dec!= '2' ):
        dec = input("Press 1 for throwing dice, press 2 for code modification ")
        print (dec)
    return dec

def vote(NUMBER_OF_PLAYERS):
    playerVotes = np.zeros(NUMBER_OF_PLAYERS)
    for x in range(NUMBER_OF_PLAYERS):
        print("player: ", x, "vote")
        while (playerVotes[x] != 1 and playerVotes[x]!= 2 ):
            try:
                playerVotes[x] = input("Press 1 for voting no, press 2 for voting yes ")
            except:
                print("Wrong input")
    return playerVotes


def print_points(playerPoints,NUMBER_OF_PLAYERS ):
    for x in range(NUMBER_OF_PLAYERS):
        print("player: ", x , "has points: ",playerPoints[x]  )
if __name__ == '__main__':
    while (winner==-1):
            importlib.reload(game)
            print("Turn of player: ", whoseTurn)
            playerVotes = np.zeros(NUMBER_OF_PLAYERS)
            decision = make_decision()
            if decision == '1':
                try:
                    playerPoints, winner = game.mainfunction(NUMBER_OF_PLAYERS, playerPoints, whoseTurn, decision, playerVotes)
                except:
                    print("An exception occurred")
            else:
                print("Change code!!!")
                playerVotes=vote(NUMBER_OF_PLAYERS)
                vote_result= calculate_vote_result(playerVotes, NUMBER_OF_PLAYERS)
                if vote_result == True:
                    input("save code and press Enter")
                try:
                    playerPoints, winner = game.mainfunction(NUMBER_OF_PLAYERS,playerPoints, whoseTurn, decision, playerVotes)
                except:
                    print("An exception occurred")
            print_points(playerPoints,NUMBER_OF_PLAYERS )
            #time.sleep(1)
            whoseTurn= (whoseTurn+1 )%NUMBER_OF_PLAYERS
    print("winner is" , winner)